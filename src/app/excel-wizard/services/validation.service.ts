import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PageInfo} from "../model/PageInfo";
import {Observable} from "rxjs";
import {Data, Rows} from "../model/ExcelStructure";
import {ExcelDocument} from "../model/ExcelDocument";
import {ApiResponse} from "../../models/ApiResponse";

@Injectable({
  providedIn: 'root'
})
export class ValidationService {


  private baseUrl = 'http://localhost:8088/excel/excel_validation';

  constructor(private http: HttpClient) {
  }

  get(pageInfo: PageInfo): Observable<ApiResponse<Data>> {
    return this.http.post<ApiResponse<Data>>(this.baseUrl, pageInfo);
  }

  update(document: ExcelDocument): Observable<ApiResponse<Rows>> {
    return this.http.put<ApiResponse<Rows>>(this.baseUrl + '/update_excel_document', document);
  }


}
