import {ChangeDetectionStrategy, Component} from '@angular/core';
import {LoaderService} from "./services/loader.service";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dynamic_excel_validation';

  constructor(public loaderService: LoaderService) {
  }
}
