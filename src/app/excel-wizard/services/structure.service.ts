import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {Header, HeaderResp} from "../model/header";
import {ApiResponse} from "../../models/ApiResponse";

@Injectable({
  providedIn: 'root'
})
export class StructureService {

  private baseUrl = 'http://localhost:8088/excel/excel_structure';

  constructor(private http: HttpClient, private router: Router) {
  }

  upload(excelHeader: Header[], excelName: String): Observable<ApiResponse<string>> {
    return this.http.post<ApiResponse<string>>(this.baseUrl + "/" + excelName, excelHeader);
  }

  get(excelName: string): Observable<ApiResponse<HeaderResp>> {
    let params = new HttpParams();
    params = params.append('excelName', excelName);
    return this.http.get<ApiResponse<HeaderResp>>(this.baseUrl, {params: params});
  }

  delete(excelName: string): Observable<ApiResponse<string>> {
    let params = new HttpParams();
    params = params.append('excelName', excelName);
    return this.http.delete<ApiResponse<string>>(this.baseUrl, {params: params});
  }

}
