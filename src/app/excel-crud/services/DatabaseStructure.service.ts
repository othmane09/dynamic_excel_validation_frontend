import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DatabaseStructure} from "../model/DatabaseStructure";
import {ColumnValidation} from "../model/ColumnValidation";
import {ExcelTable} from "../model/ExcelTable";
import {TableValidation} from "../model/TableValidation";
import {OriginExcelTable} from "../model/OriginExcelTable";
import {ApiResponse} from "../../models/ApiResponse";

@Injectable({
  providedIn: 'root'
})
export class DatabaseStructureService {

  private baseUrl = 'http://localhost:8088/excel';

  constructor(private http: HttpClient) {
  }

  getDatabaseStructure(): Observable<DatabaseStructure> {
    return this.http.get<DatabaseStructure>(this.baseUrl + '/datatable_structure');
  }

  getTableValidation(): Observable<ApiResponse<TableValidation[]>> {
    return this.http.get<ApiResponse<TableValidation[]>>('http://localhost:8088/table_validations/basic_dto');
  }

  getExcelTables(): Observable<ApiResponse<OriginExcelTable[]>> {
    return this.http.get<ApiResponse<OriginExcelTable[]>>('http://localhost:8088/excel_tables/global_dto');
  }

  getExcelTable(tableId: string): Observable<ApiResponse<OriginExcelTable>> {
    return this.http.get<ApiResponse<OriginExcelTable>>('http://localhost:8088/excel_tables/global_dto/' + tableId);
  }

  deleteExcelTables(tableId: string): Observable<ApiResponse<string>> {
    return this.http.delete<ApiResponse<string>>('http://localhost:8088/excel_tables/' + tableId);
  }

  saveExcelTable(excelTable: ExcelTable): Observable<ApiResponse<ExcelTable>> {
    return this.http.post<ApiResponse<ExcelTable>>(this.baseUrl, excelTable);
  }

  getColumnValidation(): Observable<ApiResponse<ColumnValidation[]>> {
    return this.http.get<ApiResponse<ColumnValidation[]>>('http://localhost:8088/column_validations/basic_dto');
  }

}
