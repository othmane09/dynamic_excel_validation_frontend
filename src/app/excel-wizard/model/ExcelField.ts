export class ExcelField {
  id?: string;
  name?: string;
  position?: number;
}
