import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse} from "../../models/ApiResponse";

@Injectable({
  providedIn: 'root'
})
export class ImportService {

  private baseUrl = 'http://localhost:8088/excel/import';

  constructor(private http: HttpClient) {
  }

  upload(ExcelData: FormData): Observable<ApiResponse<string>> {
    return this.http.post<ApiResponse<string>>(this.baseUrl, ExcelData);
  }

}
