import {Component, ElementRef, OnInit} from '@angular/core';
import {DatabaseStructureService} from "../services/DatabaseStructure.service";
import {Columns, DatabaseStructure, Schema, Table} from "../model/DatabaseStructure";
import {ExcelColumn, ExcelInfoColumnForeignKey, ExcelInfoColumnUniqueKey, ExcelTable} from '../model/ExcelTable';
import {ExcelDocument} from "../../excel-wizard/model/ExcelDocument";
// @ts-ignore
import * as _ from 'lodash';
import {ColumnValidation} from "../model/ColumnValidation";
import {TableValidation} from "../model/TableValidation";
import {ActivatedRoute, Router} from "@angular/router";
import {OriginExcelTable} from "../model/OriginExcelTable";
import {ToasterService} from "../../services/toaster.service";

@Component({
  selector: 'app-excel-create-update',
  templateUrl: 'excel-create-update.component.html',
})
export class ExcelCreateUpdateComponent implements OnInit {
  position: number[] = [];
  originExcelTable ?: OriginExcelTable;
  columnValidation: ColumnValidation[] = [];
  tableValidation: TableValidation[] = [];
  tableId: string = '';
  datatableHeader?: any;
  databaseStructure: DatabaseStructure = new DatabaseStructure();
  excelTable: ExcelTable = new ExcelTable();
  editableDatatableRows: Map<number, ExcelColumn> = new Map<number, ExcelColumn>();

  constructor(private databaseStructureService: DatabaseStructureService,
              private elementRef: ElementRef,
              private toasterService: ToasterService,
              private router: Router,
              private route: ActivatedRoute) {
    this.tableId = this.route.snapshot.paramMap.get('id') ?? '';
  }

  ngOnInit(): void {
    if (this.tableId != '') this.initOriginExcelTable(this.tableId);
    this.initDatasource();
    this.initColumnValidation();
    this.initTableValidations();
  }

  initDatasource() {
    this.databaseStructureService.getDatabaseStructure()
      .subscribe((res) => {
        this.databaseStructure = res;
      })
  }

  initColumnValidation() {
    this.databaseStructureService.getColumnValidation()
      .subscribe((res) => {
        this.columnValidation = res.data ?? [];
        this.initDataHeaders();
      })
  }

  initDataHeaders() {
    this.datatableHeader = [
      {name: 'name', type: 'input', data: ''},
      {name: 'position', type: 'dropDown', data: this.position, label: ''},
      {name: 'required', type: 'checkbox', data: ''},
      {name: 'foreignKey', type: 'checkbox', data: ''},
      {name: 'uniqueKey', type: 'checkbox', data: ''},
      {name: 'excelColumnValidation', type: 'dropDown', data: this.columnValidation, label: 'name'}
    ];
  }

  initTableValidations() {
    this.databaseStructureService.getTableValidation()
      .subscribe((res) => {
        this.tableValidation = res.data ?? [];
        if (this.tableId == '' || this.originExcelTable == undefined) {
          this.tableValidation.forEach((validation) => {
            this.excelTable.excelValidation.push(validation.id);
          })
          this.triggerClickByClassName("refresh-view");
        } else {
          this.convertOriginExcelToExcelTable();
        }
      })
  }


  initOriginExcelTable(tableId: string = '') {
    this.databaseStructureService.getExcelTable(tableId)
      .subscribe((res) => {
        this.originExcelTable = res.data;
      })
  }


  convertOriginExcelToExcelTable() {
    this.excelTable.id = this.originExcelTable?.id ?? '';
    this.excelTable.externalServerInsert = this.originExcelTable?.externalServerInsert ?? false;
    this.excelTable.url = this.originExcelTable?.url ?? '';
    this.excelTable.name = this.originExcelTable?.name ?? '';
    this.excelTable.description = this.originExcelTable?.description ?? '';
    this.originExcelTable?.tableValidations.forEach((validation) => {
      this.excelTable.excelValidation.push(validation.id);
    });

    this.originExcelTable?.excelColumns.forEach((column) => {
      let tempExcelColumn = new ExcelColumn();
      tempExcelColumn.excelColumnValidation = column.columnValidations[0].name;
      if (column.columnForeignKey != undefined) {
        tempExcelColumn.foreignKey = true;
        tempExcelColumn.excelInfoColumnForeignKey = column.columnForeignKey;
      }
      if (column.columnUniqueKey != undefined) {
        tempExcelColumn.uniqueKey = true;
        tempExcelColumn.excelInfoColumnUniqueKey = column.columnUniqueKey;
      }
      tempExcelColumn.id = column.id;
      tempExcelColumn.name = column.name;
      tempExcelColumn.position = column.position;
      tempExcelColumn.required = column.required;
      this.position.push(column.position);
      this.excelTable.excelInfoColumns.push(tempExcelColumn);
    });
    this.triggerClickByClassName("refresh-view");
  }

  addExcelColumn() {
    let tempExcelColumn = new ExcelColumn();
    let maxColumnPosition = this.excelTable.excelInfoColumns.sort(function (a, b) {
      return b.position - a.position;
    });
    let maxPosition: number = (maxColumnPosition.length == 0) ? 1 : maxColumnPosition[0].position + 1;
    this.position.push(maxPosition);
    tempExcelColumn.id = this.getUniqueId(2);
    tempExcelColumn.position = maxPosition;
    this.excelTable.excelInfoColumns.unshift(tempExcelColumn);
    setTimeout(() => {
      this.triggerClickByClassName('column-edit-0');
    }, 50);
  }

  removeExcelColumn(excelColumn: ExcelColumn) {
    const index = this.excelTable.excelInfoColumns.indexOf(excelColumn);
    this.excelTable.excelInfoColumns.splice(index, 1);
  }

  async submitForm() {
    if (this.validateExcelTable().length == 0) {
      this.databaseStructureService.saveExcelTable(this.excelTable)
        .subscribe({
          next: (res) => {
            this.toasterService.successMessageWithoutTranslation(res.message)
            this.router.navigate(['/list']);
          },
          error: (error) => {
            this.toasterService.errorMessageWithoutTranslation(error.error.message);
          }
        });
    }
  }

  triggerClickByClassName(className: string) {
    const elements = this.elementRef.nativeElement.getElementsByClassName(className);
    if (elements && elements.length > 0) {
      const element = elements[0];
      element.click();
    }
  }

  onRemoveColumn(excelColumn: ExcelColumn, index: number) {
    this.excelTable.excelInfoColumns.splice(index, 1);
    this.excelTable.excelInfoColumns.forEach((value, iteration) => {
      if (excelColumn.position < this.excelTable.excelInfoColumns[iteration].position) {
        this.excelTable.excelInfoColumns[iteration].position = this.excelTable.excelInfoColumns[iteration].position - 1;
      }
    });
    this.position.splice(this.position.length - 1, 1);
  }

  onRowEditInit(excelColumn: ExcelColumn, index: number) {
    this.editableDatatableRows.set(index, _.merge({}, excelColumn));
  }

  onRowEditSave(excelColumn: ExcelColumn, index: number) {
    if (this.editableDatatableRows.get(index) == undefined || this.editableDatatableRows == undefined)
      return;

    if (excelColumn.position < 1 || !this.position.includes(excelColumn.position) || !this.isNumeric(excelColumn.position))
      this.excelTable.excelInfoColumns[index].position = this.editableDatatableRows.get(index)?.position ?? 1;

    if (this.excelTable.excelInfoColumns.filter(x => x.position == excelColumn.position).length > 1) {
      this.excelTable.excelInfoColumns.forEach((value, iteration) => {
        if (value.id != excelColumn.id && value.position == excelColumn.position)
          this.excelTable.excelInfoColumns[iteration].position = this.editableDatatableRows.get(index)?.position ?? 1;
      });
    }
    this.editableDatatableRows.delete(index);
  }

  onRowEditCancel(excelDocument: ExcelDocument, index: number) {
    if (this.editableDatatableRows.get(index) == undefined || this.editableDatatableRows == undefined)
      return;
    this.excelTable.excelInfoColumns[index] = <ExcelColumn>this.editableDatatableRows.get(index);
    this.editableDatatableRows.delete(index);
  }

  isNumeric = (val: any): boolean => {
    return !isNaN(Number(val));
  }

  convertObjectToArray(object: any) {
    const Array = [];
    Array.push(object);
    return Array;
  }

  updateCheckBox(excelColumn: ExcelColumn, column: string, index: number) {
    if (column == "foreignKey")
      this.updateForeignKeyKey(excelColumn, index);
    if (column == "uniqueKey")
      this.updateUniqueKey(excelColumn, index);
  }

  updateForeignKeyKey(excelColumn: ExcelColumn, index: number) {
    if (!excelColumn.foreignKey) {
      excelColumn.excelInfoColumnForeignKey = undefined;
    } else {
      excelColumn.excelInfoColumnForeignKey = new ExcelInfoColumnForeignKey();
      setTimeout(() => {
        this.clickExpandedColumnIfItsClosed(index);
      }, 100)
    }
  }

  updateUniqueKey(excelColumn: ExcelColumn, index: number) {
    if (!excelColumn.uniqueKey) {
      excelColumn.excelInfoColumnUniqueKey = undefined;
    } else {
      excelColumn.excelInfoColumnUniqueKey = new ExcelInfoColumnUniqueKey();
      setTimeout(() => {
        this.clickExpandedColumnIfItsClosed(index);
      }, 100)
    }
  }

  clickExpandedColumnIfItsClosed(index: number) {
    const elements = this.elementRef.nativeElement.getElementsByClassName('column-expanded-' + index);
    if (
      elements[0] &&
      elements[0].firstChild &&
      typeof elements[0].firstChild.className === 'string'
    ) {
      let className = elements[0].firstChild.className;
      if (className.includes('pi-chevron-right')) {
        const element = elements[0];
        element.click();
      }
    }
  }

  isForeignKeyExist(excelColumn: ExcelColumn): boolean {
    return excelColumn != undefined && excelColumn.excelInfoColumnForeignKey != undefined;
  }

  isUniqueKeyExist(excelColumn: ExcelColumn): boolean {
    return excelColumn != undefined && excelColumn.excelInfoColumnUniqueKey != undefined;
  }

  isForeignKeyOrUniqueKeyExist(excelColumn: ExcelColumn): boolean {
    return this.isForeignKeyExist(excelColumn) || this.isUniqueKeyExist(excelColumn)
  }

  getTablesBySchema(schema: string) {
    let tables: Table[] = [];
    this.databaseStructure.schemas.forEach((value) => {
      if (value.schemaName == schema)
        tables = value.tables;
    });
    return tables;
  }

  getColumnsByTableNameAndSchema(schema: string, table: string) {
    let columns: Columns[] = [];
    this.databaseStructure.schemas.forEach((currentSchema) => {
      if (currentSchema.schemaName == schema)
        currentSchema.tables.forEach((currentTable) => {
          if (currentTable.tableName == table)
            columns = currentTable.columns;
        });
    });
    return columns;
  }

  validateExcelTable(): string[] {
    let errors: string[] = [];
    if (this.excelTable.name == undefined || this.excelTable.name.length < 2)
      errors.push("table name should be at least 2 char ");

    this.excelTable.excelInfoColumns.forEach((column) => {
      this.tableColumnValidation(column).forEach((error) => errors.push(error));
    });

    this.showErrorsFromArrays(errors);
    return errors
  }


  tableColumnValidation(excelColumn: ExcelColumn): string[] {
    let errors: string[] = [];

    if (excelColumn.name == undefined || excelColumn.name.length < 2)
      errors.push("column name should be at least 2 char in column with position  " + excelColumn.position);

    if (excelColumn.position == undefined || this.excelTable.excelInfoColumns.filter(x => x.position == excelColumn.position).length > 1)
      errors.push("there at least 2 column with the position " + excelColumn.position);

    if (this.columnValidation != undefined && !this.columnValidation.some(regex => regex.name === excelColumn.excelColumnValidation))
      errors.push("column validation not valid in column with position  " + excelColumn.position);

    this.foreignKeyValidation(excelColumn).forEach((error) => errors.push(error));

    this.uniqueKeyValidation(excelColumn).forEach((error) => errors.push(error));

    return errors;
  }

  foreignKeyValidation(excelColumn: ExcelColumn): string[] {
    let errors: string[] = [];

    if (!excelColumn.foreignKey)
      return [];

    if (excelColumn.excelInfoColumnForeignKey == undefined)
      errors.push("foreign key error undefined in column with position  " + excelColumn.position);

    this.validDatabaseStructure(
      excelColumn.excelInfoColumnForeignKey?.schema,
      excelColumn.excelInfoColumnForeignKey?.tableName,
      excelColumn.excelInfoColumnForeignKey?.tableReferenceColumn,
      excelColumn.position).forEach((error) => errors.push(error));

    return errors;
  }

  uniqueKeyValidation(excelColumn: ExcelColumn): string[] {
    let errors: string[] = [];

    if (!excelColumn.uniqueKey)
      return [];

    if (excelColumn.excelInfoColumnUniqueKey == undefined)
      errors.push("unique Key error undefined in column with position  " + excelColumn.position);

    this.validDatabaseStructure(
      excelColumn.excelInfoColumnUniqueKey?.schema,
      excelColumn.excelInfoColumnUniqueKey?.tableName,
      excelColumn.excelInfoColumnUniqueKey?.tableReferenceColumn,
      excelColumn.position).forEach((error) => errors.push(error));

    return errors;
  }

  validDatabaseStructure(schema: string | undefined,
                         table: string | undefined,
                         field: string | undefined,
                         position: number): string[] {

    let errors: string[] = [];
    if (schema == undefined || table == undefined || field == undefined) {
      errors.push("database error in column  with position  " + position);
      return errors;
    }

    let tempSchema: Schema[] = this.databaseStructure.schemas.filter(structureSchema => structureSchema.schemaName === schema);
    if (tempSchema.length == 0) {
      errors.push("schema name not valid in column with position  " + position);
      return errors;
    }

    let tempTable: Table[] = tempSchema[0].tables.filter(structureTable => structureTable.tableName === table);
    if (tempSchema.length == 0) {
      errors.push("table name not valid in column with position  " + position);
      return errors;
    }

    let tempColumn: Columns[] = tempTable[0].columns.filter(structureColumn => structureColumn.columnName === field);
    if (tempColumn.length == 0) {
      errors.push("Column not valid in column with position  " + position);
      return errors;
    }

    return errors;
  }

  showErrorsFromArrays(errors: string[]) {
    errors.forEach((error) => this.toasterService.errorMessageWithoutTranslation(error, 5000));
  }

  getUniqueId(parts: number): string {
    const stringArr = [];
    for (let i = 0; i < parts; i++) {
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      stringArr.push(S4);
    }
    return stringArr.join('-');
  }
}


