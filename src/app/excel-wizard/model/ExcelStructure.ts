import {ExcelDocument} from "./ExcelDocument";


export class Info {
  body?: ExcelDocument[];
  foreignKeys?: Map<string, string[] | any[]> = new Map<string, string[] | any[]>();
  uniqueKeysTable?: Map<string, string[] | any[]> = new Map<string, string[] | any[]>();
  uniqueKeysExcelFile?: Map<string, string[] | any[]> = new Map<string, string[] | any[]>();
  externalServerInsert: boolean = false;
  header!: string[];
}

export class Data {
  currentPage?: number;
  totalItems?: number;
  totalPages?: number;
  info?: Info;
}

export class Rows {
  rows?: ExcelDocument[];
}
