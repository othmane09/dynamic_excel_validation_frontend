import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ValidationComponent} from "./excel-wizard/validation/validation.component";
import {ImportComponent} from "./excel-wizard/import/import.component";
import {StructureComponent} from "./excel-wizard/structure/structure.component";
import {ExcelCreateUpdateComponent} from "./excel-crud/excel-create-update/excel-create-update.component";
import {ExcelWizardComponent} from "./excel-wizard/excel-wizard.component";
import {ExcelListComponent} from "./excel-crud/excel-list/excel-list.component";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {DropdownModule} from "primeng/dropdown";
import {ButtonModule} from "primeng/button";
import {MenuModule} from "primeng/menu";
import {InputTextareaModule} from "primeng/inputtextarea";
import {ScrollPanelModule} from "primeng/scrollpanel";
import {SelectButtonModule} from "primeng/selectbutton";
import {PanelMenuModule} from "primeng/panelmenu";
import {BreadcrumbModule} from "primeng/breadcrumb";
import {MegaMenuModule} from "primeng/megamenu";
import {TabMenuModule} from "primeng/tabmenu";
import {TreeTableModule} from "primeng/treetable";
import {TagModule} from "primeng/tag";
import {AvatarModule} from "primeng/avatar";
import {TreeModule} from "primeng/tree";
import {ToastModule} from "primeng/toast";
import {PanelModule} from "primeng/panel";
import {ListboxModule} from "primeng/listbox";
import {TableModule} from "primeng/table";
import {ChipsModule} from "primeng/chips";
import {CardModule} from "primeng/card";
import {AccordionModule} from "primeng/accordion";
import {ChipModule} from "primeng/chip";
import {PasswordModule} from "primeng/password";
import {MultiSelectModule} from "primeng/multiselect";
import {DividerModule} from "primeng/divider";
import {ToolbarModule} from "primeng/toolbar";
import {KnobModule} from "primeng/knob";
import {SkeletonModule} from "primeng/skeleton";
import {AutoCompleteModule} from "primeng/autocomplete";
import {TreeSelectModule} from "primeng/treeselect";
import {ToggleButtonModule} from "primeng/togglebutton";
import {FileUploadModule} from "primeng/fileupload";
import {ContextMenuModule} from "primeng/contextmenu";
import {MenubarModule} from "primeng/menubar";
import {CalendarModule} from "primeng/calendar";
import {InputMaskModule} from "primeng/inputmask";
import {InputNumberModule} from "primeng/inputnumber";
import {CheckboxModule} from "primeng/checkbox";
import {StepsModule} from "primeng/steps";
import {SplitterModule} from "primeng/splitter";
import {TabViewModule} from "primeng/tabview";
import {InputTextModule} from "primeng/inputtext";
import {FieldsetModule} from "primeng/fieldset";
import {BadgeModule} from "primeng/badge";
import {AvatarGroupModule} from "primeng/avatargroup";
import {RatingModule} from "primeng/rating";
import {ScrollTopModule} from "primeng/scrolltop";
import {SplitButtonModule} from "primeng/splitbutton";
import {DialogModule} from "primeng/dialog";
import {LoaderComponent} from "./loader/loader.component";
import {ConvertObjectToArray} from "./pipes/convertObjectToArray.pipe";
import {GetMapArrayValueByIndexPipe} from "./pipes/getMapArrayValueByIndex.pipe";
import {MessageService} from "primeng/api";
import {LoaderInterceptor} from "./interceptors/loader.interceptor";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";


const PRIMNG_MODULES = [
  ButtonModule,
  DropdownModule,
  PanelModule,
  PanelMenuModule,
  ScrollPanelModule,
  MultiSelectModule,
  InputTextModule,
  InputTextareaModule,
  SplitButtonModule,
  AutoCompleteModule,
  CheckboxModule,
  SelectButtonModule,
  ToggleButtonModule,
  TreeSelectModule,
  ListboxModule,
  CalendarModule,
  InputNumberModule,
  ChipsModule,
  RatingModule,
  KnobModule,
  InputMaskModule,
  PasswordModule,
  TreeModule,
  TreeTableModule,
  TableModule,
  SplitterModule,
  DividerModule,
  MenuModule,
  FieldsetModule,
  TabMenuModule,
  TabViewModule,
  AccordionModule,
  ToolbarModule,
  MegaMenuModule,
  StepsModule,
  BreadcrumbModule,
  MenubarModule,
  ContextMenuModule,
  SelectButtonModule,
  FileUploadModule,
  SkeletonModule,
  ChipModule,
  TagModule,
  AvatarModule,
  BadgeModule,
  AvatarGroupModule,
  ScrollTopModule,
  DialogModule,
  ToastModule,
  CardModule,
];

@NgModule({
  declarations: [
    AppComponent,
    ExcelWizardComponent,
    ValidationComponent,
    StructureComponent,
    ImportComponent,
    ExcelCreateUpdateComponent,
    ExcelListComponent,
    LoaderComponent,
    ConvertObjectToArray,
    GetMapArrayValueByIndexPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    DragDropModule,
    ...PRIMNG_MODULES
  ],
  providers: [MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
