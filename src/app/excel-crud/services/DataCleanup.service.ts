import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse} from "../../models/ApiResponse";

@Injectable({
  providedIn: 'root'
})
export class DataCleanupService {

  private baseUrl = 'http://localhost:8088/data_cleanup';

  constructor(private http: HttpClient) {
  }

  cleanUpFiledAndDocuments(): Observable<ApiResponse<string>> {
    return this.http.get<ApiResponse<string>>(this.baseUrl);
  }

}
