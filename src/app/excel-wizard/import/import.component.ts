import {Component, OnInit} from '@angular/core';
import {WizardService} from "../services/wizard.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ExcelStatus} from "../model/status";
import {Subject, takeUntil} from "rxjs";
import {ImportService} from "../services/import.service";
import {ToasterService} from "../../services/toaster.service";
import {ApiResponse} from "../../models/ApiResponse";


@Component({
  selector: 'app-excel-import',
  templateUrl: 'import.component.html',
})
export class ImportComponent implements OnInit {
  tableName: string = '';
  form!: FormGroup;
  isSubmitted = false;
  endSubscription$: Subject<any> = new Subject();
  uploadedFiles!: any;

  constructor(private formBuilder: FormBuilder,
              private wizardService: WizardService,
              private importService: ImportService,
              private router: Router,
              private route: ActivatedRoute,
              private toasterService: ToasterService) {
  }

  get excelImportForm() {
    return this.form.controls;
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('tableName') == null)
      return;
    this.tableName = this.route.snapshot.paramMap.get('tableName') ?? "";
    this.wizardService.checkIfCurrentExcelTableStepIsValid(this.tableName, ExcelStatus.IMPORTATION);
    this._initForm();
  }

  ngOnDestroy() {
    this.endSubscription$.next('');
    this.endSubscription$.complete();
  }

  onFileUpload(event: { files: FormData[] }) {
    this.uploadedFiles = event.files[0];
    if (this.uploadedFiles)
      this.form.patchValue({file: this.uploadedFiles});
  }


  onSubmit() {
    console.log("(uploadHandler)=\"myUploader($event)\"")
    this.isSubmitted = true;
    if (this.form.invalid) return;
    const excelImportFormData = new FormData();
    Object.keys(this.excelImportForm).map((key) => {
      excelImportFormData.append(key, this.excelImportForm[key].value);
    });
    this._addExcelImport(excelImportFormData);
  }

  private _initForm() {
    this.form = this.formBuilder.group({
      excelName: [this.tableName, Validators.required],
      file: ['', Validators.required]
    });
  }

  private _addExcelImport(excelData: FormData) {
    this.importService
      .upload(excelData)
      .pipe(takeUntil(this.endSubscription$))
      .subscribe({
          next: (response: ApiResponse<string>) => {
            this.wizardService.nextPage(this.tableName, ExcelStatus.IMPORTATION);
          },
          error: (err) => {
            this.toasterService.errorMessageWithoutTranslation(err.error.message)
          },
          complete: () => {
          }
        }
      );
  }

}
