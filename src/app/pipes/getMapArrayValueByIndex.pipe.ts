import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'getMapArrayValueByIndexPipe'})

export class GetMapArrayValueByIndexPipe implements PipeTransform {
  transform(map: Map<string, string[]> | undefined, index: string): { foreignKey: string; }[] {
    if (map == undefined || !map.has(index))
      return [];
    else
      return map?.get(index)?.map(item => ({foreignKey: item})) ?? [];
  }
}
