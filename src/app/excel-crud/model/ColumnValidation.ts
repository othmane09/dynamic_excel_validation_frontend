export class ColumnValidation {
  id: string = '';
  name: string = '';
  type: string = '';
  pattern: string = '';
  description: string = '';
}
