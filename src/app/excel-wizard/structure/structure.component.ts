import {Component, ElementRef, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";
import {Subject, takeUntil} from "rxjs";
import {FormGroup} from "@angular/forms";
import {StructureService} from "../services/structure.service";
import {WizardService} from "../services/wizard.service";
import {ExcelStatus} from "../model/status";
import {Header} from "../model/header";
import {ActivatedRoute} from "@angular/router";
import {ToasterService} from "../../services/toaster.service";

@Component({
  selector: 'app-excel-structure',
  templateUrl: 'structure.component.html',
  styleUrls: ['structure.component.scss']
})
export class StructureComponent implements OnInit {
  tableName: string = '';
  excelColumns: Header[] = [];
  excelFields: Header[] = [];
  form!: FormGroup;
  submitExcelHeaders = false;
  endSubscription$: Subject<any> = new Subject();
  excelHeaderMaxPosition !: number;

  constructor(private structureService: StructureService,
              private wizardService: WizardService,
              private toasterService: ToasterService,
              private route: ActivatedRoute,
              public refreshData: ElementRef) {
  }


  ngOnInit() {
    if (this.route.snapshot.paramMap.get('tableName') == null)
      return;
    this.tableName = this.route.snapshot.paramMap.get('tableName') ?? "";
    this.wizardService.checkIfCurrentExcelTableStepIsValid(this.tableName, ExcelStatus.EDIT_STRUCTURE);
    this._initHeaders();
  }

  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      console.log(event.container.data)
    }
  }

  onSubmit() {
    this.submitExcelHeaders = true;
    this._updateExcelHeaders(this.excelFields);
  }

  nextPage() {
    this.wizardService.nextPage(this.tableName, ExcelStatus.EDIT_STRUCTURE)
  }

  addExcelHeader() {
    this.excelFields.push(
      {
        name: "Empty Case",
        position: this.excelHeaderMaxPosition + 1
      }
    )
  }

  delete(name: string) {
    this.structureService
      .delete(name)
      .subscribe({
        next: (response) => {
          this.toasterService.successMessageWithoutTranslation("excel file deleted successfully")
          this.wizardService.checkIfCurrentExcelTableStepIsValid(this.tableName, ExcelStatus.EDIT_CONTENT);
        },
        error: (err) => {
          this.toasterService.errorMessageWithoutTranslation(err.error.message)
        }
      });
  }

  private _initHeaders() {
    this.structureService.get(this.tableName).subscribe(
      {
        next: (res) => {
          // @ts-ignore
          this.excelFields = res.data?.excelFields?.sort((a, b) => a.position ?? 0 - b.position ?? 0) ?? [];
          // @ts-ignore
          this.excelColumns = res.data?.excelColumns?.sort((a, b) => a.position - b.position);
          // @ts-ignore
          this.excelHeaderMaxPosition = Math.max.apply(Math, this.excelFields.map(function (o) {
            return o.position;
          }));
          this.refreshData.nativeElement.click();
        },
        error: () => {
          this.toasterService.errorMessage('SORRY_ERROR_OCCURRED_DURING_THIS_PROCESS_TRY_AGAIN_LATER')
        }
      }
    );
  }

  private _updateExcelHeaders(excelData: Header[]) {
    this.structureService
      .upload(excelData, this.tableName)
      .pipe(takeUntil(this.endSubscription$))
      .subscribe({
          next: () => {
            this.nextPage()
          },
          error: () => {
            this.toasterService.errorMessage('SORRY_ERROR_OCCURRED_DURING_THIS_PROCESS_TRY_AGAIN_LATER')
          }
        }
      );
  }
}
