export class ExcelTable {
  id?: string;
  name: string = '';
  description: string = '';
  url: string = '';
  externalServerInsert: boolean = false;
  excelValidation: any[] = [];
  excelInfoColumns: ExcelColumn[] = [];
}

export class ExcelColumn {
  id: string = '';
  name: string = '';
  position: number = 1;
  required: boolean = true;
  foreignKey: boolean = false;
  excelInfoColumnForeignKey ?: ExcelInfoColumnForeignKey;
  uniqueKey: boolean = false;
  excelInfoColumnUniqueKey ?: ExcelInfoColumnUniqueKey;
  excelColumnValidation ?: string;
}

export class ExcelInfoColumnForeignKey {
  schema?: string;
  tableName?: string;
  tableReferenceColumn?: string;
  tableIdColumn?: string;
}

export class ExcelInfoColumnUniqueKey {
  schema?: string;
  tableName?: string;
  tableReferenceColumn?: string;
}
