import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {ExcelWizardComponent} from "./excel-wizard/excel-wizard.component";
import {ImportComponent} from "./excel-wizard/import/import.component";
import {StructureComponent} from "./excel-wizard/structure/structure.component";
import {ValidationComponent} from "./excel-wizard/validation/validation.component";
import {ExcelListComponent} from "./excel-crud/excel-list/excel-list.component";
import {ExcelCreateUpdateComponent} from "./excel-crud/excel-create-update/excel-create-update.component";

export const routes: Route[] = [
  {
    path: "list",
    component: ExcelListComponent
  },
  {
    path: "create_update",
    component: ExcelCreateUpdateComponent
  },
  {
    path: "create_update/:id",
    component: ExcelCreateUpdateComponent
  },
  {
    path: 'wizard',
    component: ExcelWizardComponent, children: [
      {path: '', redirectTo: 'import/:tableName', pathMatch: 'full'},
      {path: 'import/:tableName', component: ImportComponent},
      {path: 'structure/:tableName', component: StructureComponent},
      {path: 'validation/:tableName', component: ValidationComponent}
    ]
  },
  {path: '**', redirectTo: 'list', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
