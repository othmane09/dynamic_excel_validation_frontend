export class Status {
  status!: string;
}

export enum ExcelStatus {
  IMPORTATION = "IMPORTATION",
  EDIT_STRUCTURE = "EDIT_STRUCTURE",
  EDIT_CONTENT = "EDIT_CONTENT",
  VALID_DATA = "VALID_DATA",
}
