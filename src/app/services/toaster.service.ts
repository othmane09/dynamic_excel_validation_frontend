import {Injectable} from '@angular/core';
import {MessageService} from "primeng/api";

@Injectable({
  providedIn: 'root',
})
export class ToasterService {

  defaultLifeTime = 2000;

  constructor(private messageService: MessageService) {
  }

  successMessage(message: string, prefix: string = 'TOASTER', time: number = this.defaultLifeTime) {
    this.messageService.add({
      severity: 'success',
      summary: "SUCCESS",
      detail: message,
      life: time
    })
  }

  successMessageWithoutTranslation(message: string, time: number = this.defaultLifeTime) {
    this.messageService.add({
      severity: 'success',
      summary: 'SUCCESS',
      detail: message,
      life: time
    })
  }

  warningMessageWithoutTranslation(message: string, time: number = this.defaultLifeTime) {
    this.messageService.add({
      severity: 'warning',
      summary: '',
      detail: message,
      life: time
    })
  }

  errorMessage(message: string, prefix: string = 'TOASTER', time: number = this.defaultLifeTime) {
    this.messageService.add({
      severity: 'error',
      summary: "ERROR",
      detail: message,
      life: time
    })
  }

  errorMessageWithoutTranslation(message: string, time: number = this.defaultLifeTime) {
    this.messageService.add({
      severity: 'error',
      summary: 'ERROR',
      detail: message,
      life: time
    })
  }

}
