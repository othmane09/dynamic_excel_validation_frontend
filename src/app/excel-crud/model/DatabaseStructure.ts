export class DatabaseStructure {
  schemas: Schema[] = [];
}

export class Schema {
  schemaName: string = '';
  tables: Table[] = [];
}

export class Table {
  tableName: string = '';
  columns: Columns[] = [];
}

export class Columns {
  columnName: string = '';
}
