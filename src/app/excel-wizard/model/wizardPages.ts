export class WizardPages {
  url: string = '';
  haveNextPage: boolean = true
  nextPage: string = '';
  havePrevPage: boolean = true
  prevPage: string = '';

  constructor(url: string, haveNextPage: boolean, nextPage: string, havePrevPage: boolean, prevPage: string) {
    this.url = url ?? '';
    this.haveNextPage = haveNextPage ?? true;
    this.nextPage = nextPage ?? '';
    this.havePrevPage = havePrevPage ?? true;
    this.prevPage = prevPage ?? '';
  }

}
