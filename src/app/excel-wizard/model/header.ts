export class Header {
  id?: string;
  name?: string;
  position?: number;
}

export class HeaderResp {
  excelFields !: Header[];
  excelColumns !: Header[];
}
