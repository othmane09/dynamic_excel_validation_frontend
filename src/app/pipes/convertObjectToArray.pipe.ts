import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'ConvertObjectToArray'
})
export class ConvertObjectToArray implements PipeTransform {
  transform(object: any): any[] {
    const Array = [];
    Array.push(object);
    return Array;
  }
}
