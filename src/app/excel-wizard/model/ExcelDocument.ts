export class ExcelDocument {
  id: string = '';
  contents: Map<string, string[]> = new Map<string, string[]>();
  foreignKeys: Map<string, string[]> = new Map<string, string[]>();
  uniqueKeys: Map<string, string[]> = new Map<string, string[]>();
  columnValidation: Map<string, string[]> = new Map<string, string[]>();
}
