### dynamic excel validation
****** 
- This is an application that gives you the ability validate your Excel file automatically just by given the structure of the file like columns anf foreign keys 
- when the Excel is valid you have 2 choice 
- 1 create your own backend web service and send this valid rows to him 
- 2 download the valid rows


- ### Installation dependencies
******
- NPM
- angular 14

- ### Some dependencies chosen 
******
- primeng :  UI components for Angular 
- primeFlex :  is a lightweight responsive CSS 
- file-saver  : manage excel exportation 



#### Backend Application
****** 
- go visit : https://gitlab.com/othmane09/dynamic_excel_validation_backend
