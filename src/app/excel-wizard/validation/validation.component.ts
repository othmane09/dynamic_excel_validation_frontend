import {Component, ElementRef, OnInit} from '@angular/core';
import {ValidationService} from "../services/validation.service";
import {StructureService} from "../services/structure.service";
import {WizardService} from "../services/wizard.service";
import {ExcelStatus} from "../model/status";
import {LazyLoadEvent} from "primeng/api";
import {PageInfo} from "../model/PageInfo";
import {Subject, takeUntil} from "rxjs";
import {Data, Rows} from "../model/ExcelStructure";
import {ExcelDocument} from "../model/ExcelDocument";
// @ts-ignore
import * as _ from 'lodash';
import {ActivatedRoute} from "@angular/router";
import {InsertDataService} from "../services/insert-data.service";
// @ts-ignore
import {saveAs} from 'file-saver';
import {ToasterService} from "../../services/toaster.service";
import {ApiResponse} from "../../models/ApiResponse";

@Component({
  selector: 'app-excel-validation',
  templateUrl: 'validation.component.html',
  styleUrls: ['validation.component.scss']
})
export class ValidationComponent implements OnInit {
  tableName: string = '';
  endSubscription$: Subject<any> = new Subject();
  datatableBody: any;
  datatableHeader: any;
  externalServerInsert: boolean = false;
  foreignKeys?: Map<string, string[]> = new Map<string, string[]>();
  foreignKeysFilterOptions: Map<string, string[]> = new Map<string, string[]>();
  uniqueKeys: string[] = [];
  uniqueKeysTable?: Map<string, string[]> = new Map<string, string[]>();
  uniqueKeysExcelFile?: Map<string, string[]> = new Map<string, string[]>();
  uniqueKeysExcelFileGroupBy: Map<string, Map<string, number>> = new Map<string, Map<string, number>>();
  editableDatatableRows: Map<number, ExcelDocument> = new Map<number, ExcelDocument>();
  totalRecords: number = 0;
  firstLoad = true;

  constructor(private readonly validationService: ValidationService,
              private readonly structureService: StructureService,
              private readonly wizardService: WizardService,
              private readonly toasterService: ToasterService,
              private insertDataService: InsertDataService,
              private route: ActivatedRoute,
              public refreshData: ElementRef) {
  }


  ngOnInit() {
    if (this.route.snapshot.paramMap.get('tableName') == null)
      return;
    this.tableName = this.route.snapshot.paramMap.get('tableName') ?? "";
    this.wizardService.checkIfCurrentExcelTableStepIsValid(this.tableName, ExcelStatus.EDIT_CONTENT);

  }

  loadFromServer(event: LazyLoadEvent) {
    this.validationService
      .get(this.getPageInformation(event))
      .pipe(takeUntil(this.endSubscription$))
      .subscribe({
        next: (response: ApiResponse<Data>) => {
          if (!(response.data == undefined || response.data.info == undefined)) {
            this.updateDatatableExtraInfo(response.data);
            this.datatableBody = response.data.info.body ?? [''];
          }
          this.totalRecords = response.data?.totalItems ?? 0;
          this.refreshData.nativeElement.click();
        },
        error: (err) => {
          this.datatableBody = [];
          this.totalRecords = 0;
          if (err.error.status == 400)
            this.toasterService.errorMessageWithoutTranslation(err.error.message);
          else
            this.toasterService.successMessageWithoutTranslation("All data are up to inserted ");
        }
      });
  }

  getPageInformation(event: LazyLoadEvent) {
    let pageInfo: PageInfo = new PageInfo(event.rows ?? 10, (event.first ?? 0) / (event.rows ?? 10), this.tableName);
    if (event.globalFilter != null && event.globalFilter.value != undefined)
      pageInfo.globalFilter = event.globalFilter.value;
    if (event.filters != undefined) {
      new Map(Object.entries(event.filters)).forEach((elem: any, key: any) => {
        let tempVariable = (elem.value as HTMLInputElement).value;
        if (tempVariable != null && tempVariable != "") {
          pageInfo.filter[key] = tempVariable;
        }
      });
    }
    return pageInfo
  }

  updateDatatableExtraInfo(data: Data) {

    if (data == undefined || data.info == undefined || !this.firstLoad)
      return;

    const {header, foreignKeys, uniqueKeysTable, uniqueKeysExcelFile, externalServerInsert} = data.info;
    this.datatableHeader = header;
    this.foreignKeys = new Map(Object.entries(foreignKeys ?? ['']));
    this.uniqueKeysTable = new Map(Object.entries(uniqueKeysTable ?? ['']));
    this.uniqueKeysExcelFile = new Map(Object.entries(uniqueKeysExcelFile ?? ['']));
    this.externalServerInsert = externalServerInsert;
    new Map(Object.entries(uniqueKeysExcelFile ?? [''])).forEach((value: any, key: string) => {
      this.uniqueKeys.push(key);
      this.uniqueKeysExcelFileGroupBy.set(key, new Map(Object.entries(_.countBy(this.uniqueKeysExcelFile?.get(key), function (e: any) {
        return e;
      }))));
    })
    this.initForeignKeysFilterOptions();
    this.firstLoad = false;
  }

  initForeignKeysFilterOptions() {
    this.foreignKeys?.forEach((value, key) => {
      this.foreignKeysFilterOptions.set(key, value.slice(0, 20));
    })
  }

  onRowEditInit(excelDocument: ExcelDocument, index: number) {
    this.editableDatatableRows.set(index, _.merge({}, excelDocument));
  }

  onRowEditSave(excelDocument: ExcelDocument, index: number) {

    let oldExcelDocumentData = this.editableDatatableRows.get(index);
    this.validationService.update(excelDocument).subscribe({
      next: (response: ApiResponse<Rows>) => {
        if (response.data == undefined || response.data.rows == undefined || response.data.rows[0] == undefined || response.data.rows[0]["contents"] == undefined || oldExcelDocumentData == undefined)
          return;
        let responseRow = response.data.rows[0];

        this.updateUniqueKeysExcelFileGroupBy(oldExcelDocumentData, responseRow);
        this.datatableBody.forEach((datatableRow: any, datatableKey: any) => {
          if (responseRow.id == datatableRow.id) {
            this.datatableBody[datatableKey] = responseRow;
          } else {
            if (this.uniqueKeys == undefined)
              return;
            this.uniqueKeys.forEach((key: any) => {
              // @ts-ignore
              this.datatableBody[datatableKey]["uniqueKeys"] = this.getNewUniqueKeyValidation(this.datatableBody[datatableKey], responseRow, oldExcelDocumentData, key);
            });
          }
          this.refreshData.nativeElement.click();
        });
      },
      error: (err) => {
        this.toasterService.errorMessageWithoutTranslation(err.error.message);
      }
    });
    this.editableDatatableRows.delete(index);
  }

  updateUniqueKeysExcelFileGroupBy(oldDatatableRow: ExcelDocument, newDatatableRow: ExcelDocument) {

    if (this.uniqueKeys == undefined)
      return;
    this.uniqueKeys.forEach((key: any) => {
      // @ts-ignore
      if (oldDatatableRow['contents'][key] == newDatatableRow['contents'][key])
        return;
      // @ts-ignore
      let countOldRowUniqueKeyIteration = this.uniqueKeysExcelFileGroupBy.get(key).get(oldDatatableRow['contents'][key]);
      // @ts-ignore
      if (this.uniqueKeysExcelFileGroupBy.get(key).has(newDatatableRow['contents'][key])) {
        // @ts-ignore
        let countNewRowUniqueKeyIteration = this.uniqueKeysExcelFileGroupBy.get(key).get(newDatatableRow['contents'][key]);
        // @ts-ignore
        this.uniqueKeysExcelFileGroupBy.get(key).set(newDatatableRow['contents'][key], ((countNewRowUniqueKeyIteration == 0) ? 1 : countNewRowUniqueKeyIteration + 1));
      } else {
        // @ts-ignore
        this.uniqueKeysExcelFileGroupBy.get(key).set(newDatatableRow['contents'][key], 1);
      }
      // @ts-ignore
      this.uniqueKeysExcelFileGroupBy.get(key).set(oldDatatableRow['contents'][key], ((countOldRowUniqueKeyIteration == 0) ? 0 : countOldRowUniqueKeyIteration - 1));
    });

  }

  getNewUniqueKeyValidation(datatableRow: ExcelDocument, updatedDatatableRow: ExcelDocument, oldDatatableRow: ExcelDocument, uniqueKey: string) {
    // @ts-ignore
    if (datatableRow["contents"][uniqueKey] == updatedDatatableRow["contents"][uniqueKey]) {
      // @ts-ignore
      if (!this.checkIfListHaveValue(datatableRow["uniqueKeys"], uniqueKey)) {
        if (datatableRow["uniqueKeys"] == null)
          return [uniqueKey];
        else { // @ts-ignore
          return [...datatableRow["uniqueKeys"], uniqueKey];
        }
      }
    } else {
      // @ts-ignore
      if (datatableRow["contents"][uniqueKey] == oldDatatableRow["contents"][uniqueKey]) {
        // @ts-ignore
        if (!this.checkIfListHaveValue(this.uniqueKeysTable[uniqueKey], oldDatatableRow["contents"][uniqueKey]) && this.uniqueKeysExcelFileGroupBy.get(uniqueKey).get(oldDatatableRow['contents'][uniqueKey]) <= 1) {
          // @ts-ignore
          this.removeElementFromStringArray(datatableRow["uniqueKeys"], uniqueKey);
        }
      }
    }
    // @ts-ignore
    return datatableRow["uniqueKeys"];
  }

  removeElementFromStringArray(list: string[], element: string) {
    if (list == undefined)
      return [''];
    list.forEach((value, index) => {
      if (value == element) list.splice(index, 1);
    });
    return list;
  }

  onRowEditCancel(excelDocument: ExcelDocument, index: number) {
    this.datatableBody[index] = this.editableDatatableRows.get(index);
    this.editableDatatableRows.delete(index);
  }

  checkIfHeaderInForeignKey(columnName: string): boolean {
    return !(this.foreignKeys == undefined || !this.foreignKeys.has(columnName));

  }

  getForeignKeyValueByColumnName(columnName: string): string[] {
    if (this.foreignKeys == undefined || !this.foreignKeys.has(columnName))
      return [''];
    else
      return this.foreignKeys?.get(columnName) ?? [''];
  }

  checkIfListHaveValue(list: string[], value: string): boolean {
    if (list == null || value == null)
      return false;
    return list.includes(value);
  }

  delete(name: string) {
    this.structureService
      .delete(name)
      .subscribe({
        next: (response) => {
          this.toasterService.successMessageWithoutTranslation("excel file deleted successfully")
          this.wizardService.checkIfCurrentExcelTableStepIsValid(this.tableName, ExcelStatus.EDIT_CONTENT);
        },
        error: (err) => {
          this.toasterService.errorMessageWithoutTranslation(err.error.message)
        }
      });
  }

  insertValidData() {
    this.insertDataService
      .insertValidData(this.tableName)
      .subscribe({
        next: (response) => {
          this.toasterService.successMessageWithoutTranslation(response.message, 3000);
          setTimeout(() => {
            window.location.reload();
          }, 1000)
        },
        error: (err) => {
          this.toasterService.errorMessageWithoutTranslation(err.error.message)
        }
      });
  }

  downloadValidData() {
    this.insertDataService
      .downloadValidData(this.tableName)
      .subscribe({
        next: (response) => {
          saveAs(response, `script.sql`);
          setTimeout(() => {
            window.location.reload();
          }, 1000)
        },
        error: (err) => {
          this.toasterService.errorMessageWithoutTranslation(err.error.message)
        }
      })
  }

}
