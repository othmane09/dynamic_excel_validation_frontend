export class PageInfo {

  public size?: number;
  public tableName?: string;
  public page?: number;
  public globalFilter?: string;
  public filter: { [index: string]: string } = {};

  constructor(size: number, page: number, tableName: string) {
    this.page = page;
    this.size = size;
    this.tableName = tableName;
  }
}
