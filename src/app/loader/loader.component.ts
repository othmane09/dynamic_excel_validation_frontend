import {Component, OnInit} from '@angular/core';
import {gsap} from "gsap";
// @ts-ignore
import {MotionPathPlugin} from "gsap/MotionPathPlugin.js";

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
    this.gsapAnimation2();
  }

  gsapAnimation() {
    gsap.registerPlugin(MotionPathPlugin);
    let boxSize = window.innerHeight / 2 - 100;
    gsap.to(".loading_header", {
      transformOrigin: "center " + 250 + "px",
      rotation: 360,
      duration: 100,
      ease: "none",
      repeat: -1
    });

    gsap.to(".column-2", 1, {
      scale: 0,
      y: 60,
      yoyo: true,
      ease: "Power4.easeInOut",
      repeat: -1,
      duration: 5,
      delay: 0,
    });
    gsap.to(".column-3", 1, {
      scale: 0,
      y: 60,
      yoyo: true,
      ease: "Power4.easeInOut",
      repeat: -1,
      duration: 5,
      delay: 5,
    });
    gsap.to(".column-4", 1, {
      scale: 0,
      y: 60,
      yoyo: true,
      ease: "Power4.easeInOut",
      repeat: -1,
      duration: 5,
      delay: 10,
    });
    gsap.to(".column-5", 1, {
      scale: 0,
      y: 60,
      yoyo: true,
      ease: "Power4.easeInOut",
      repeat: -1,
      duration: 5,
      delay: 15,
    });
    gsap.to(".column-1", 1, {
      scale: 0,
      y: 60,
      yoyo: true,
      ease: "Power4.easeInOut",
      repeat: -1,
      duration: 5,
      delay: 20,
    });


  }


  gsapAnimation2() {
    gsap.registerPlugin(MotionPathPlugin);
    let boxSize = window.innerHeight / 2;
    gsap.to(".loading_header", {
      transformOrigin: "center " + boxSize + "px",
      rotation: 360,
      duration: 5,
      ease: "none",
      repeat: -1
    });
    gsap.to(".loading_content", {
      rotation: -360,
      duration: 5,
      ease: "none",
      repeat: -1
    });
    gsap.to(gsap.utils.toArray(".box"), 1, {
      scale: 0,
      y: 60,
      yoyo: true,
      ease: "power2.inOut",
      delay: 2,
      repeat: -1,
      stagger: {amount: 1, grid: "auto", from: "end"}
    });
  }
}
