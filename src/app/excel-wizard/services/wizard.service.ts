import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {ExcelStatus, Status} from "../model/status";
import {Router} from "@angular/router";
import {WizardPages} from "../model/wizardPages";
import {ToasterService} from "../../services/toaster.service";
import {ApiResponse} from "../../models/ApiResponse";

@Injectable({
  providedIn: 'root'
})
export class WizardService {

  wizardMapper = new Map<ExcelStatus, WizardPages>();
  private baseUrl = 'http://localhost:8088/excel/excel_structure';
  private wizardRoute = '/wizard/';

  constructor(private http: HttpClient, private router: Router, private toasterService: ToasterService) {
    this.initWizardMapper();
  }

  initWizardMapper() {
    this.wizardMapper.set(ExcelStatus.IMPORTATION, new WizardPages('import', true, 'structure', false, ''));
    this.wizardMapper.set(ExcelStatus.EDIT_STRUCTURE, new WizardPages('structure', true, 'validation', true, 'import'));
    this.wizardMapper.set(ExcelStatus.EDIT_CONTENT, new WizardPages('validation', false, 'valid_data', true, 'structure'));
  }

  getCurrentExcelTableWizardStep(excelName: string): Observable<ApiResponse<Status>> {
    let params = new HttpParams().append('excelName', excelName);
    return this.http.get<ApiResponse<Status>>(this.baseUrl + '/status', {params: params});
  }

  checkIfCurrentExcelTableStepIsValid(excelName: string, sourcePage: ExcelStatus) {
    this.getCurrentExcelTableWizardStep(excelName)
      .subscribe({
        next: (response) => {
          if (response.data == undefined) {
            this.toasterService.errorMessage('SORRY_ERROR_OCCURRED_DURING_THIS_PROCESS_TRY_AGAIN_LATER')
            this.router.navigate(['/wizard/' + excelName]);
            return;
          }
          this.redirect(excelName, response.data.status as ExcelStatus, sourcePage);
        },
        error: (err) => {
          this.toasterService.errorMessageWithoutTranslation(err.error.message, 5000);
          this.router.navigate(["/"]);
        }
      });
  }

  redirect(excelName: string, status: ExcelStatus, sourcePage: ExcelStatus) {
    if (status == sourcePage)
      return;
    if ((status == ExcelStatus.VALID_DATA && sourcePage == ExcelStatus.EDIT_CONTENT)
      || (status == ExcelStatus.EDIT_CONTENT && sourcePage == ExcelStatus.VALID_DATA))
      return;
    this.router.navigate([this.wizardRoute + this.wizardMapper.get(status)?.url + '/' + excelName]);
  }


  nextPage(excelName: string, status: ExcelStatus) {
    if (this.wizardMapper.get(status)?.haveNextPage)
      this.router.navigate([this.wizardRoute + this.wizardMapper.get(status)?.nextPage + '/' + excelName]);
    else
      this.toasterService.errorMessage('SORRY_ERROR_OCCURRED_DURING_THIS_PROCESS_TRY_AGAIN_LATER')
  }

  prevPage(excelName: string, status: ExcelStatus) {
    if (this.wizardMapper.get(status)?.havePrevPage)
      this.router.navigate([this.wizardRoute + this.wizardMapper.get(status)?.prevPage + '/' + excelName]);
    else
      this.toasterService.errorMessage('SORRY_ERROR_OCCURRED_DURING_THIS_PROCESS_TRY_AGAIN_LATER')
  }


}
