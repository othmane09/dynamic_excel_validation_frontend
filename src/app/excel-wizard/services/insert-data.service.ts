import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ApiResponse} from "../../models/ApiResponse";

@Injectable({
  providedIn: 'root'
})
export class InsertDataService {

  private baseUrl = 'http://localhost:8088/excel/excel_insert_valid_data';

  constructor(private http: HttpClient) {
  }

  insertValidData(excelName: string): Observable<ApiResponse<string>> {
    return this.http.get<ApiResponse<string>>(this.baseUrl + '/external_api/' + excelName);
  }

  downloadValidData(excelName: string): Observable<any> {
    // @ts-ignore
    return this.http.get<any>(this.baseUrl + '/download/' + excelName, {responseType: 'blob'});
  }
}
