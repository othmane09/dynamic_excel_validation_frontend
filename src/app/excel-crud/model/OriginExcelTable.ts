export class OriginExcelTable {
  id: string = '';
  name: string = '';
  url: string = '';
  externalServerInsert: boolean = false;
  description: string = '';
  tableValidations: Validation[] = [];
  excelColumns: ExcelColumn[] = [];
}

export class Validation {
  id: string = '';
  name: string = '';
  description: string = '';
}

export class ExcelColumn {
  id: string = '';
  name: string = '';
  description: string = '';
  required: boolean = true;
  position: number = 1;
  columnForeignKey ?: ColumnForeignKey;
  columnUniqueKey ?: ColumnUniqueKey;
  columnValidations: Validation[] = [];
}

export class ColumnForeignKey {
  id: string = '';
  schema: string = '';
  tableName: string = '';
  tableReferenceColumn: string = '';
  tableIdColumn: string = '';
}

export class ColumnUniqueKey {
  id: string = '';
  schema: string = '';
  tableName: string = '';
  tableReferenceColumn: string = '';
}
