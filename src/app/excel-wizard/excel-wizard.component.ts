import {Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'app-excel-wizard',
  templateUrl: 'excel-wizard.component.html',
})
export class ExcelWizardComponent implements OnInit {
  tableName: string = '';
  menuItems!: MenuItem[];

  constructor(private route: ActivatedRoute, private router: Router,) {
  }

  ngOnInit(): void {
    // console.log(this.router)
    // if(this.route.snapshot.paramMap.get('tableName')== null)
    //   return;
    // this.tableName = this.route.snapshot.paramMap.get('tableName')??"";
    // this.initMenuItems();
  }

  initMenuItems() {
    this.menuItems =
      [{
        label: 'Import',
        routerLink: 'import/' + this.tableName
      },
        {
          label: 'structure',
          routerLink: 'structure/' + this.tableName
        },
        {
          label: 'validation',
          routerLink: 'validation/' + this.tableName
        },
        {
          label: 'valid_data',
          routerLink: 'valid_data/' + this.tableName
        }
      ];
  }
}
