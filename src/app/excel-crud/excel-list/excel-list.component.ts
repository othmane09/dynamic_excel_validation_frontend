import {Component, ElementRef, OnInit} from '@angular/core';
import {DatabaseStructureService} from "../services/DatabaseStructure.service";
// @ts-ignore
import * as _ from 'lodash';
import {OriginExcelTable} from "../model/OriginExcelTable";
import {DataCleanupService} from "../services/DataCleanup.service";
import {ToasterService} from "../../services/toaster.service";

@Component({
  selector: 'app-excel-list',
  templateUrl: 'excel-list.component.html',
})
export class ExcelListComponent implements OnInit {
  originExcelTable: OriginExcelTable[] = [];
  datatableHeader = [
    {name: 'name'},
    {name: 'description'},
  ];


  constructor(private databaseStructureService: DatabaseStructureService,
              private dataCleanupService: DataCleanupService,
              private elementRef: ElementRef,
              private toasterService: ToasterService) {
  }

  ngOnInit(): void {
    this.initOriginExcelTable();
  }

  initOriginExcelTable() {
    this.databaseStructureService.getExcelTables().subscribe(
      (res) => {
        this.originExcelTable = res.data ?? [];
        this.triggerClickByClassName("refresh-view");
      }
    )
  }

  triggerClickByClassName(className: string) {
    const elements = this.elementRef.nativeElement.getElementsByClassName(className);
    if (elements && elements.length > 0) {
      const element = elements[0];
      element.click();
    }
  }

  onRemoveOriginExcelTable(originExcelTable: OriginExcelTable, index: number) {
    this.databaseStructureService.deleteExcelTables(originExcelTable.id)
      .subscribe(
        (res) => {
          this.toasterService.successMessageWithoutTranslation(res.message);
          this.initOriginExcelTable();
        }
      )
  }

  dataCleanup() {
    this.dataCleanupService.cleanUpFiledAndDocuments()
      .subscribe(
        (res) => {
          this.toasterService.successMessageWithoutTranslation(res.message);
        }
      )
  }
}


